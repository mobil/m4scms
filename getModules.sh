#!/bin/sh

basePath=`pwd`

#get latest branch revision
git pull

git clone --depth 1 git://gitorious.org/mobil/website.git
git clone --depth 1 git://gitorious.org/mobil/datamanager.git
git clone --depth 1 git://gitorious.org/mobil/dbinterface.git




################################################################################
########## submodules will used when --track capabilty will added by git developer
#print submodules
#git submodule status

#init submodules
#git submodule init
#git config -l

#finally get submodules
#git submodule update
#git submodule foreach "cd $basePath$path && git pull origin master"
#git submodule foreach "cd $basePath$path && git checkout master"
################################################################################
